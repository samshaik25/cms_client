
import React from 'react';
import { BrowserRouter , Route,Routes} from 'react-router-dom';

import './App.css';

import GetUsers from './getUsers/GetUsers';

import Navbarr from './navbar';
import GetUserInfo from './userInfo/GetUserInfo';

function App() {
  return (
    <BrowserRouter  basename='/USER'>

    <div className="App">
      <Navbarr/>
     <Route   exact path='/' component={GetUsers}>
     <GetUsers/>
     </Route>
     <Route  path='/:id' component={GetUserInfo} />

     </div>
     
     </BrowserRouter>
     
     
     );
    }
    
    // <GetUsers/>
    // <Route className="product" path='/:id' component={GetProducts} />
    export default App;
    // </Route>
    
    // <GetBrands />
  // <Route path='/:id' component={GetUserInfo} ></Route>