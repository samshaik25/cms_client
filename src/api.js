import axios from "axios";


export function fetchUsers(){

 return   axios.get("http://localhost:8000/users/getusers").then((res)=>res.data)
    
}


export function fetchInfo(id){

    return   axios.get(`http://localhost:8000/users/infoByUID/${id}`).then((res)=>res.data)

}