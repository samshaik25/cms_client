import React, { Component } from 'react';

import { Card } from 'react-bootstrap';

import { Link } from 'react-router-dom';

import '../App.css'
class User extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div className="p-4">
            <Card  calssName="m-2" bg="dark" border="dark" text="light" style={{ width: '20rem' }}>
            
            <Card.Header className=" p-2" text="success"><h3>Username :</h3></Card.Header>
            <Card.Title  className=" p-2" ><h4 > {this.props.name}</h4></Card.Title>
            
            
            </Card>
            <div>
            <Link className="Link" to={`${this.props.id}`} key={this.props.id}>
            <button    className= 'But btn btn-sm btn-warning'>explore </button>
            </Link>
            </div>
            </div>
            );
        }
    }
    
    export default User;