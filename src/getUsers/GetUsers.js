import React, { Component } from 'react';
import User from './User';

import * as API from '../api'

class GetUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users:[]
          }
    }

async getUser()
{
 const users= await API.fetchUsers()
 this.setState({users})
}
    componentDidMount()
    {
        this.getUser()
    }
    render() { 
        const {users}=this.state
        return ( 
            <div className="d-flex flex-row flex-wrap justify-content-center align-items-center">

            {users.map(({name,id})=>(
                <User key={id}  id={id} name={name} />
            ))}
            </div>
         );
    }
}
 
export default GetUsers;