import React, { Component } from 'react';

import * as API from '../api'

import Info from './Info';

class GetUserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info:[]
          }
    }
     
    async getInfo(id)
    {
        const info=await API.fetchInfo(id)
        this.setState({info})
    }

    componentDidMount()
    {
        let {id}=this.props.match.params;
       this.getInfo(id)
    }
    render() { 
        const {info}=this.state
        return ( 
            <div className="d-flex flex-row flex-wrap justify-content-center align-items-center">

            {info.map(({title,description ,id})=>(
                <Info id={id} key={id} title={title} description={description} />
            ))}
            </div>
         );
    }
}
 
export default GetUserInfo;