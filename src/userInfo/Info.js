import React, { Component } from 'react';

import { Card } from 'react-bootstrap';


class Info extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            
            <div className="p-4">
            <Card  bg="dark" border="dark" text="light" style={{ width: '20rem' }}>
            <Card.Header  className=" p-2" ><h4 className="p-2 d-flex  flex-row"><span>Title:</span>
               <h4>{this.props.title} </h4></h4></Card.Header>

        
            
            <Card.Title  className=" p-2" ><h4><span></span>{this.props.description}</h4></Card.Title>

            </Card>
            </div>
         );
    }
}
 
export default Info;